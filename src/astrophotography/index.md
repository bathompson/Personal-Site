# Astrophotography
I occasionally take amateur astrophotography pictures. They will be uploaded to this page. All pictures were taken with a Canon EOS Rebel SL2. Photos were processed with [Darktable](https://www.darktable.org/).


[![](/img/AP1.png)](/img/AP1.png)

[![](/img/AP2.png)](/img/AP2.png)

[![](/img/AP3.png)](/img/AP3.png)