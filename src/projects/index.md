---
title: Projects
layout: "base.njk"
eleventyExcludeFromCollections: true
---


{% for post in collections.projects %}

- [{{post.data.title}}]({{post.url}})
{% endfor %}